package com.github.soshibby.philips.pft5500.integration.types.interfaces;

public interface TvEventListener {

    public void onVolumeChanged(int volume);

    public void onMuteChanged(boolean mute);

    public void onChannelChanged();

}
