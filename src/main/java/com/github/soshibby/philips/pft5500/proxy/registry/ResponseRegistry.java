package com.github.soshibby.philips.pft5500.proxy.registry;

import org.glassfish.grizzly.http.server.Response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ResponseRegistry {
    private Map<String, List<Response>> responses = new HashMap<String, List<Response>>();

    public void add(String token, Response response) {
        List<Response> responseList = responses.get(token);

        if (responseList == null) {
            responseList = new ArrayList<Response>();
        }

        responseList.add(response);
        responses.put(token, responseList);
    }

    public void removeByToken(String token) {
        List<Response> responseList = responses.get(token);

        if (responseList == null) {
            return;
        }

        for (Response response : responseList) {
            response.finish();
        }

        responses.remove(token);
    }

    public List<Response> getByToken(String token) {
        return responses.get(token);
    }

}
