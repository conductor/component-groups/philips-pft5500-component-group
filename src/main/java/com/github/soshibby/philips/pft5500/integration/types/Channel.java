package com.github.soshibby.philips.pft5500.integration.types;

import java.net.URL;

public class Channel {

    private String name;
    private int number;
    private String albumArtUrl;
    private URL hdStreamUrl;
    private URL sdStreamUrl;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getAlbumArtUrl() {
        return albumArtUrl;
    }

    public void setAlbumArtUrl(String albumArtUrl) {
        this.albumArtUrl = albumArtUrl;
    }

    public URL getHdStreamUrl() {
        return hdStreamUrl;
    }

    public void setHdStreamUrl(URL hdStreamUrl) {
        this.hdStreamUrl = hdStreamUrl;
    }

    public URL getSdStreamUrl() {
        return sdStreamUrl;
    }

    public void setSdStreamUrl(URL sdStreamUrl) {
        this.sdStreamUrl = sdStreamUrl;
    }
}
