package com.github.soshibby.philips.pft5500.proxy;

import com.github.soshibby.philips.pft5500.proxy.registry.ResponseRegistry;
import com.github.soshibby.philips.pft5500.proxy.registry.TokenRegistry;
import org.glassfish.grizzly.http.server.HttpServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;
import java.util.List;

public class Server {

    private static final Logger log = LoggerFactory.getLogger(Server.class);
    private ResponseRegistry responseRegistry = new ResponseRegistry();
    private TokenRegistry tokenRegistry = new TokenRegistry();
    private RequestHandler requestHandler;
    private Thread killExpiredRequestsThread;
    private HttpServer server;

    public Server(URL remoteURL, String path, int portNumber) throws IOException {
        log.info("Starting proxy server.");
        requestHandler = new RequestHandler(remoteURL, responseRegistry, tokenRegistry);

        server = HttpServer.createSimpleServer(null, portNumber);
        server.getServerConfiguration().addHttpHandler(requestHandler, path);
        server.start();

        killExpiredRequestsThread = new Thread(new killExpiredRequests());
        killExpiredRequestsThread.start();
    }

    public TokenRegistry getTokenRegistry() {
        return this.tokenRegistry;
    }

    public void setRemoteURL(URL url) {
        requestHandler.setRemoteURL(url);
    }

    public void stop() {
        killExpiredRequestsThread.interrupt();
        server.shutdownNow();
    }

    private class killExpiredRequests implements Runnable {

        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    List<String> expiredTokens = tokenRegistry.getExpiredTokens();

                    for (String token : expiredTokens) {
                        log.info("Removing expired token: " + token);
                        responseRegistry.removeByToken(token);
                        tokenRegistry.remove(token);
                    }
                } catch (Exception e) {
                    log.error("Exception occured while running thread to kill expired requests.", e);
                }

                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    log.error("Failed to sleep thread.", e);
                }
            }
        }
    }

}
