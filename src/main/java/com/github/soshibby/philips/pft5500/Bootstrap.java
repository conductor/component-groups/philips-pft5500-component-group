package com.github.soshibby.philips.pft5500;

import com.github.soshibby.philips.pft5500.components.Decoder;
import com.github.soshibby.philips.pft5500.components.Speaker;
import com.github.soshibby.philips.pft5500.integration.services.TvService;
import org.conductor.bootstrap.IComponentGroupBootstrap;
import org.conductor.component.annotations.ComponentGroupBootstrap;
import org.conductor.component.annotations.ComponentGroupOption;
import org.conductor.component.annotations.ComponentGroupOptions;
import org.conductor.component.types.IComponent;
import org.conductor.database.Database;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

@ComponentGroupBootstrap
@ComponentGroupOptions(options = {@ComponentGroupOption(dataType = "string", name = "tv name")})
public class Bootstrap implements IComponentGroupBootstrap {

    private static final Logger log = LoggerFactory.getLogger(Bootstrap.class);

    @Override
    public Map<String, IComponent> start(Database database, Map<String, Object> componentGroupOptions, Map<String, Map<String, Object>> componentsOptions) throws Exception {
        log.info("Bootstrapping philips pft5500 component group.");

        String tvName = (String) componentGroupOptions.get("tv name");
        TvService tvService = new TvService(tvName);

        Map<String, IComponent> components = getComponents(database, componentsOptions, tvService);

        tvService.refreshDevices();
        return components;
    }

    private Map<String, IComponent> getComponents(Database database,
                                                  Map<String, Map<String, Object>> options, TvService tvService) {
        Map<String, IComponent> components = new HashMap<String, IComponent>();

        Speaker speaker = new Speaker(database, options.get("Speaker"), tvService);
        Decoder decoder = new Decoder(database, options.get("Decoder"), tvService);

        components.put("Speaker", speaker);
        components.put("Decoder", decoder);
        return components;
    }

}
