package com.github.soshibby.philips.pft5500.integration.converters.channel;

import com.github.soshibby.philips.pft5500.integration.types.Channel;
import com.google.common.base.Function;
import com.google.common.base.Throwables;
import org.fourthline.cling.support.model.DIDLObject.Property;
import org.fourthline.cling.support.model.Res;
import org.fourthline.cling.support.model.item.Item;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

public class ChannelDecoder implements Function<Item, Channel> {

    private static final Logger log = LoggerFactory.getLogger(ChannelDecoder.class);

    @Override
    public Channel apply(Item item) {
        try {
            Channel channel = new Channel();
            URI albumArtUri = (URI) getProperty(item, "albumArtURI");
            channel.setAlbumArtUrl(albumArtUri.toURL().toString());
            channel.setName((String) getProperty(item, "channelName"));
            channel.setNumber(getChannelNumber(item));
            channel.setHdStreamUrl(getStreamUrl(item, true));
            channel.setSdStreamUrl(getStreamUrl(item, false));
            return channel;
        } catch (Exception e) {
            log.error("Failed to decode channel information.", e);
            throw Throwables.propagate(e);
        }
    }

    private static int getChannelNumber(Item item) throws Exception {
        // There is no property for fetching the channel number,
        // so we have to use the albmArtURI property which contains the channel number in its uri
        // path...
        URI albumArtUri = (URI) getProperty(item, "albumArtURI");
        String albumArtPath = albumArtUri.getPath(); // Example: /channeldb/tv/channelLists/alltv/64/logo
        albumArtPath = albumArtPath.substring(0, albumArtPath.length() - 5); // Remove /logo

        int channelStartIndex = albumArtPath.lastIndexOf("/");

        if (channelStartIndex == -1) {
            throw new Exception("Unexpected channel id, couldn't find channel number.");
        }

        String channel = albumArtPath.substring(channelStartIndex + 1);
        return Integer.parseInt(channel);
    }

    private static URL getStreamUrl(Item item, boolean hdStream) throws MalformedURLException {
        for (Res resource : item.getResources()) {
            if (hdStream && resource.getProtocolInfo().getAdditionalInfo().contains("_HD_")) {
                return new URL(resource.getValue());
            } else if (!hdStream && resource.getProtocolInfo().getAdditionalInfo().contains("_SD_")) {
                return new URL(resource.getValue());
            }
        }

        return null;
    }

    private static Object getProperty(Item item, String propertyName) {
        for (Property property : item.getProperties()) {
            if (property.getDescriptorName().equals(propertyName)) {
                return property.getValue();
            }
        }

        return null;
    }
}
