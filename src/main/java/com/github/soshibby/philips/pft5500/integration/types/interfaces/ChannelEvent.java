package com.github.soshibby.philips.pft5500.integration.types.interfaces;

public class ChannelEvent {

    private int channel;

    public int getChannel() {
        return channel;
    }

    public void setChannel(int channel) {
        this.channel = channel;
    }

}
