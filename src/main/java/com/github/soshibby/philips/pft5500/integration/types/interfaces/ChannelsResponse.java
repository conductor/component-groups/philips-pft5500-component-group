package com.github.soshibby.philips.pft5500.integration.types.interfaces;

import com.github.soshibby.philips.pft5500.integration.types.Channel;

import java.util.List;

public interface ChannelsResponse {

    public void success(List<Channel> channels);

    public void failure(String message);
}
