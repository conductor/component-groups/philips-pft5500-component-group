package com.github.soshibby.philips.pft5500.integration.services;

import com.github.soshibby.philips.pft5500.integration.converters.channel.ChannelsDecoder;
import com.github.soshibby.philips.pft5500.integration.converters.speaker_event.SpeakerEventDecoder;
import com.github.soshibby.philips.pft5500.integration.types.Channel;
import com.github.soshibby.philips.pft5500.integration.types.interfaces.*;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.fourthline.cling.UpnpService;
import org.fourthline.cling.UpnpServiceImpl;
import org.fourthline.cling.controlpoint.ActionCallback;
import org.fourthline.cling.controlpoint.SubscriptionCallback;
import org.fourthline.cling.model.UnsupportedDataException;
import org.fourthline.cling.model.action.ActionArgumentValue;
import org.fourthline.cling.model.action.ActionInvocation;
import org.fourthline.cling.model.gena.CancelReason;
import org.fourthline.cling.model.gena.GENASubscription;
import org.fourthline.cling.model.gena.RemoteGENASubscription;
import org.fourthline.cling.model.message.UpnpResponse;
import org.fourthline.cling.model.message.header.STAllHeader;
import org.fourthline.cling.model.meta.*;
import org.fourthline.cling.model.types.UDAServiceId;
import org.fourthline.cling.model.types.UDAServiceType;
import org.fourthline.cling.registry.Registry;
import org.fourthline.cling.registry.RegistryListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TvService {

    private static final Logger logger = LoggerFactory.getLogger(TvService.class);
    private List<TvEventListener> listeners = new ArrayList();
    private UpnpService upnpService;
    private String deviceName;

    public TvService(String tvName) {
        this.deviceName = tvName;
        logger.info("Starting UPNP Client...");
        upnpService = new UpnpServiceImpl(listener);
    }

    public void getCurrentChannel(final CurrentChannelResponse response) {
        RemoteDevice device = getDeviceByName(this.deviceName, "ContentDirectory");

        if (device == null) {
            response.failure("Couldn't find device.");
        }

        Service service = device.findService(new UDAServiceId("ContentDirectory"));
        Action browse = service.getAction("Browse");
        ActionInvocation setTargetInvocation = new ActionInvocation(browse);
        setTargetInvocation.setInput("ObjectId", "0/TROOT/TNOWPLAYING");
        setTargetInvocation.setInput("BrowseFlag", "BrowseDirectChildren");
        setTargetInvocation.setInput("Filter", "*");
        setTargetInvocation.setInput("StartingIndex", "0");
        setTargetInvocation.setInput("RequestedCount", "1000");
        setTargetInvocation.setInput("SortCriteria", "");

        ActionCallback setTargetCallback = new ActionCallback(setTargetInvocation) {

            @Override
            public void success(ActionInvocation invocation) {
                ActionArgumentValue[] output = invocation.getOutput();
                List<Channel> channels = null;

                try {
                    channels = ChannelsDecoder.transform((String) invocation.getOutput("Result").getValue());
                } catch (Exception e) {
                    response.failure("Failed to convert channel list. " + e.getMessage());
                    return;
                }

                if (channels == null || channels.size() == 0) {
                    response.failure("Couldn't find current channel. Received 0 channels from upnp service.");
                } else {
                    response.success(channels.get(0));
                }
            }

            @Override
            public void failure(ActionInvocation invocation, UpnpResponse operation, String defaultMsg) {
                response.failure("Failed to get current channel, upnp action invokation failed. " + defaultMsg);
            }
        };

        upnpService.getControlPoint().execute(setTargetCallback);
    }

    public void getChannels(final ChannelsResponse response) {
        RemoteDevice device = getDeviceByName(this.deviceName, "ContentDirectory");

        if (device == null) {
            response.failure("Couldn't find device.");
        }

        Service service = device.findService(new UDAServiceId("ContentDirectory"));
        Action browse = service.getAction("Browse");
        ActionInvocation setTargetInvocation = new ActionInvocation(browse);
        setTargetInvocation.setInput("ObjectId", "0/TROOT/WatchTV");
        setTargetInvocation.setInput("BrowseFlag", "BrowseDirectChildren");
        setTargetInvocation.setInput("Filter", "*");
        setTargetInvocation.setInput("StartingIndex", "0");
        setTargetInvocation.setInput("RequestedCount", "1000");
        setTargetInvocation.setInput("SortCriteria", "");

        ActionCallback setTargetCallback = new ActionCallback(setTargetInvocation) {

            @Override
            public void success(ActionInvocation invocation) {
                ActionArgumentValue[] output = invocation.getOutput();
                List<Channel> channels = null;

                try {
                    channels = ChannelsDecoder.transform((String) invocation.getOutput("Result").getValue());
                } catch (Exception e) {
                    response.failure("Failed to convert channel list. " + e.getMessage());
                }

                response.success(channels);
            }

            @Override
            public void failure(ActionInvocation invocation, UpnpResponse operation, String defaultMsg) {
                response.failure("Failed to get current channel, upnp action invokation failed. " + defaultMsg);
            }
        };

        upnpService.getControlPoint().execute(setTargetCallback);
    }

    public void setChannel(int channelId) throws Exception {
        RemoteDevice device = getDeviceByName(this.deviceName, "ContentDirectory");

        if (device == null) {
            throw new Exception("Couldn't find device.");
        }

        String host = device.getIdentity().getDescriptorURL().getHost();
        int port = device.getIdentity().getDescriptorURL().getPort();

        String url = String.format("http://%s:%s/content/720/A_T_%s.ts", host, port, channelId);

        logger.info("Changing channel, sending request to: {}", url);

        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpget = new HttpGet(url);

        CloseableHttpResponse resp = httpclient.execute(httpget);
        resp.close();
    }

    public void setMute(boolean mute, final Response response) {
        Device device = getDeviceByName(this.deviceName, "RenderingControl");
        Service service = device.findService(new UDAServiceId("RenderingControl"));
        Action volumeService = service.getAction("SetVolume");
        ActionInvocation setTargetInvocation = new ActionInvocation(volumeService);
        setTargetInvocation.setInput("InstanceId", "0");
        setTargetInvocation.setInput("Channel", "Master");
        setTargetInvocation.setInput("DesiredMute", mute);

        ActionCallback setTargetCallback = new ActionCallback(setTargetInvocation) {

            @Override
            public void success(ActionInvocation invocation) {
                response.success();
            }

            @Override
            public void failure(ActionInvocation invocation, UpnpResponse operation, String defaultMsg) {
                response.failure(defaultMsg);
            }
        };

        upnpService.getControlPoint().execute(setTargetCallback);
    }

    public void setVolume(int volume, final Response response) {
        Device device = getDeviceByName(this.deviceName, "RenderingControl");

        if (device == null) {
            response.failure("No device found!");
        }

        Service service = device.findService(new UDAServiceId("RenderingControl"));
        Action volumeService = service.getAction("SetVolume");
        ActionInvocation setTargetInvocation = new ActionInvocation(volumeService);
        setTargetInvocation.setInput("InstanceId", "0");
        setTargetInvocation.setInput("Channel", "Master");
        setTargetInvocation.setInput("DesiredVolume", String.valueOf(volume));

        ActionCallback setTargetCallback = new ActionCallback(setTargetInvocation) {

            @Override
            public void success(ActionInvocation invocation) {
                response.success();
            }

            @Override
            public void failure(ActionInvocation invocation, UpnpResponse operation, String defaultMsg) {
                response.failure(defaultMsg);
            }
        };

        upnpService.getControlPoint().execute(setTargetCallback);
    }

    public void listen() {
        logger.info("Starting listening for events.");
        listenForChannelEvents();
        listenForSpeakerEvents();
    }

    private void listenForChannelEvents() {
        Device device = getDeviceByName(this.deviceName, "ContentDirectory");
        Service service = device.findService(new UDAServiceId("ContentDirectory"));

        SubscriptionCallback callback = new SubscriptionCallback(service, 600) {

            @Override
            public void established(GENASubscription sub) {
                logger.info("Established channel events listener: " + sub.getSubscriptionId());
            }

            @Override
            protected void failed(GENASubscription subscription, UpnpResponse responseStatus, Exception exception, String defaultMsg) {
                logger.error("Failed to listen for channel events. " + defaultMsg);
            }

            @Override
            public void ended(GENASubscription sub, CancelReason reason, UpnpResponse response) {
            }

            @Override
            public void eventReceived(GENASubscription sub) {
                logger.info("Received channel changed event.");

                for (TvEventListener listener : listeners) {
                    listener.onChannelChanged();
                }
            }

            @Override
            public void eventsMissed(GENASubscription sub, int numberOfMissedEvents) {
                logger.info("Missed channel changed events: " + numberOfMissedEvents);
            }

            @Override
            protected void invalidMessage(RemoteGENASubscription sub, UnsupportedDataException ex) {
                logger.error("Invalid channel event.", ex);
            }
        };

        upnpService.getControlPoint().execute(callback);
    }

    private void listenForSpeakerEvents() {
        Device device = getDeviceByName(this.deviceName, "RenderingControl");
        Service service = device.findService(new UDAServiceId("RenderingControl"));

        SubscriptionCallback callback = new SubscriptionCallback(service, 600) {

            @Override
            public void established(GENASubscription sub) {
                logger.info("Established speaker event listener: " + sub.getSubscriptionId());
            }

            @Override
            protected void failed(GENASubscription subscription, UpnpResponse responseStatus, Exception exception, String defaultMsg) {
                logger.error("Failed to listen to speaker event, {}.", exception);
            }

            @Override
            public void ended(GENASubscription sub, CancelReason reason, UpnpResponse response) {
            }

            @Override
            public void eventReceived(GENASubscription sub) {
                logger.info("Received speaker event.");

                try {
                    SpeakerEvent event = SpeakerEventDecoder.transform(sub.getCurrentValues());

                    for (TvEventListener listener : listeners) {
                        listener.onMuteChanged(event.getMute());
                        listener.onVolumeChanged(event.getVolume());
                    }
                } catch (JAXBException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void eventsMissed(GENASubscription sub, int numberOfMissedEvents) {
                logger.error("Missed speaker events: " + numberOfMissedEvents);
            }

            @Override
            protected void invalidMessage(RemoteGENASubscription sub, UnsupportedDataException ex) {
                logger.error("Invalid speaker event message.", ex);
            }
        };

        upnpService.getControlPoint().execute(callback);
    }

    private RemoteDevice getDeviceByName(String name, String serviceType) {
        Collection<Device> devices = upnpService.getRegistry().getDevices(new UDAServiceType(serviceType));

        for (Device device : devices) {
            if (device.getDetails().getFriendlyName().equalsIgnoreCase(name)) {
                return (RemoteDevice) device;
            }
        }

        return null;
    }

    public void refreshDevices() {
        // Send a search message to all devices and services, they should respond soon
        upnpService.getControlPoint().search(new STAllHeader());
    }

    public void shutdown() {
        upnpService.shutdown();
    }

    private RegistryListener listener = new RegistryListener() {

        @Override
        public void remoteDeviceDiscoveryStarted(Registry registry, RemoteDevice device) {
            logger.info("Discovery started: " + device.getDisplayString() + " - " + device.getDetails().getFriendlyName());
        }

        @Override
        public void remoteDeviceDiscoveryFailed(Registry registry, RemoteDevice device, Exception ex) {
            logger.info("Discovery failed: " + device.getDisplayString() + " => " + ex);
        }

        @Override
        public void remoteDeviceAdded(Registry registry, RemoteDevice device) {
            logger.info("Remote device available: {} - {}", device.getDetails().getFriendlyName(), device.getDisplayString());
            if (device.getDetails().getFriendlyName().equalsIgnoreCase(deviceName)) {
                switch (device.getDisplayString()) {
                    case "Philips TV DMR 2k15MTK":
                        listenForSpeakerEvents();
                        break;
                    case "Royal Philips Electronics Philips TV Server 2k15MTK":
                        listenForChannelEvents();
                        break;
                }
            }
        }

        @Override
        public void remoteDeviceUpdated(Registry registry, RemoteDevice device) {
            //logger.info("Remote device updated: " + device.getDisplayString());
        }

        @Override
        public void remoteDeviceRemoved(Registry registry, RemoteDevice device) {
            logger.info("Remote device removed: " + device.getDisplayString());
        }

        @Override
        public void localDeviceAdded(Registry registry, LocalDevice device) {
            logger.info("Local device added: " + device.getDisplayString());
        }

        @Override
        public void localDeviceRemoved(Registry registry, LocalDevice device) {
            logger.info("Local device removed: " + device.getDisplayString());
        }

        @Override
        public void beforeShutdown(Registry registry) {
            logger.info("Before shutdown, the registry has devices: " + registry.getDevices().size());
        }

        @Override
        public void afterShutdown() {
            logger.info("Shutdown of registry complete!");
        }
    };

    public void addListener(TvEventListener listener) {
        listeners.add(listener);
    }

    public void removeListener(TvEventListener listener) {
        listeners.remove(listener);
    }
}
