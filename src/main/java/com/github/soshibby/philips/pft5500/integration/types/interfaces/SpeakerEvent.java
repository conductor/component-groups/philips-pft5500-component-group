package com.github.soshibby.philips.pft5500.integration.types.interfaces;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SpeakerEvent {

    private int volume;
    private boolean mute;

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public int getVolume() {
        return this.volume;
    }

    public boolean getMute() {
        return mute;
    }

    public void setMute(boolean mute) {
        this.mute = mute;
    }

}
