package com.github.soshibby.philips.pft5500.components;

import com.github.soshibby.philips.pft5500.integration.services.TvService;
import com.github.soshibby.philips.pft5500.integration.types.Channel;
import com.github.soshibby.philips.pft5500.integration.types.interfaces.CurrentChannelResponse;
import com.github.soshibby.philips.pft5500.integration.types.interfaces.TvEventListener;
import com.github.soshibby.philips.pft5500.proxy.Server;
import com.github.soshibby.philips.pft5500.proxy.utils.DateUtil;
import com.github.soshibby.philips.pft5500.proxy.utils.IPAddress;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.conductor.component.annotations.Property;
import org.conductor.component.annotations.ReadOnlyProperty;
import org.conductor.component.types.Component;
import org.conductor.component.types.DataType;
import org.conductor.database.Database;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

@org.conductor.component.annotations.Component(type = "decoder")
@ReadOnlyProperty(name = "streamUrl", dataType = DataType.STRING, defaultValue = "")
public class Decoder extends Component {

    private static final Logger log = LoggerFactory.getLogger(Decoder.class);
    private TvService tvService;
    private Server proxy;
    private Thread updateTokenThread;
    private String PATH = "/stream";
    private int PORT_NUMBER = 7000;

    public Decoder(Database database, Map<String, Object> options, TvService tvService) {
        super(database, options);

        this.updateTokenThread = new Thread(new updateToken());
        this.updateTokenThread.start();

        this.tvService = tvService;
        this.tvService.addListener(tvEventListener);

    }

    @Property(initialValue = "0")
    public void setChannel(Integer channel) throws Exception {
        tvService.setChannel(channel);
    }

    private TvEventListener tvEventListener = new TvEventListener() {

        @Override
        public void onVolumeChanged(int volume) {

        }

        @Override
        public void onMuteChanged(boolean mute) {

        }

        @Override
        public void onChannelChanged() {
            tvService.getCurrentChannel(currentChannel);
        }

    };

    private CurrentChannelResponse currentChannel = new CurrentChannelResponse() {

        @Override
        public void success(Channel channel) {
            try {
                URL remoteURL = channel.getHdStreamUrl();
                proxy.setRemoteURL(remoteURL);

                updatePropertyValue("channel", channel.getNumber());
            } catch (Exception e) {
                reportError(e.getMessage(), ExceptionUtils.getStackTrace(e));
            }
        }

        @Override
        public void failure(String message) {
            log.error("Failed to get current channel. {}", message);
        }
    };

    private class updateToken implements Runnable {

        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                URL proxyURL = null;
                String token = null;

                // Create new token
                try {
                    token = createToken();
                    proxyURL = getProxyURL(token);
                } catch (IOException e) {
                    reportError("Failed to get external ip.");
                }

                // Start proxy server (if it's not already running)
                try {
                    if (proxy == null) {
                        URL dummyURL = new URL("http://localhost:40000"); // We don't know the url to the stream that is running on the tv, so we just set this to a random url. (the url will be updated when we discover the tv)
                        proxy = new Server(dummyURL, PATH, PORT_NUMBER);
                    }
                } catch (IOException e) {
                    reportError("Failed to start proxy server. " + e.getMessage());
                }

                // Add token to the list of valid tokens
                proxy.getTokenRegistry().add(token, DateUtil.addMinutes(new Date(), 10));

                // Update the streamUrl property with the new token
                try {
                    updatePropertyValue("streamUrl", proxyURL.toString());
                } catch (Exception e) {
                    reportError("Failed to update stream url property. " + e.getMessage());
                }

                // Sleep
                try {
                    Thread.sleep(60000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    @Override
    public void destroy() {
        updateTokenThread.interrupt();
        proxy.stop();
    }

    private URL getProxyURL(String token) throws IOException {
        String externalIP = IPAddress.getExternalIP();
        return new URL("http://" + externalIP + ":" + PORT_NUMBER + PATH + "?token=" + token);
    }

    private String createToken() {
        return UUID.randomUUID().toString();
    }
}
