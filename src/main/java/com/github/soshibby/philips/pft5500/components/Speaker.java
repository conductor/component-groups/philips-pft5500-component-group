package com.github.soshibby.philips.pft5500.components;

import com.github.soshibby.philips.pft5500.integration.services.TvService;
import com.github.soshibby.philips.pft5500.integration.types.interfaces.Response;
import com.github.soshibby.philips.pft5500.integration.types.interfaces.TvEventListener;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.conductor.component.annotations.Property;
import org.conductor.component.types.Component;
import org.conductor.database.Database;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

@org.conductor.component.annotations.Component(type = "speaker")
public class Speaker extends Component {

    private static final Logger log = LoggerFactory.getLogger(Speaker.class);
    private TvService tvService;

    public Speaker(Database database, Map<String, Object> options, TvService tvService) {
        super(database, options);
        this.tvService = tvService;
        this.tvService.addListener(tvEventListener);
    }

    @Property(initialValue = "0")
    public void setVolume(Integer volume) {
        tvService.setVolume(volume, new Response() {

            @Override
            public void success() {
                log.info("Successfully changed volume.");
            }

            @Override
            public void failure(String message) {
                log.error("Failed to change volume. {}", message);
            }
        });
    }

    @Property(initialValue = "false")
    public void setMute(Boolean mute) {
        tvService.setMute(mute, new Response() {

            @Override
            public void success() {
                log.info("Successfully changed mute on tv.");
            }

            @Override
            public void failure(String message) {
                log.error("Failed to change mute on tv. {}", message);
            }
        });
    }

    private TvEventListener tvEventListener = new TvEventListener() {
        @Override
        public void onVolumeChanged(int volume) {
            try {
                updatePropertyValue("volume", volume);
            } catch (Exception e) {
                reportError(e.getMessage(), ExceptionUtils.getStackTrace(e));
            }
        }

        @Override
        public void onMuteChanged(boolean mute) {
            try {
                updatePropertyValue("mute", mute);
            } catch (Exception e) {
                reportError(e.getMessage(), ExceptionUtils.getStackTrace(e));
            }
        }

        @Override
        public void onChannelChanged() {

        }
    };
}
