package com.github.soshibby.philips.pft5500.integration.types.interfaces;

public interface Response {

    public void success();

    public void failure(String message);

}
