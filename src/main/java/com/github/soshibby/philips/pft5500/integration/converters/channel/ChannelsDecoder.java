package com.github.soshibby.philips.pft5500.integration.converters.channel;

import com.github.soshibby.philips.pft5500.integration.types.Channel;
import com.google.common.collect.Lists;
import org.fourthline.cling.support.contentdirectory.DIDLParser;
import org.fourthline.cling.support.model.DIDLContent;

import java.util.ArrayList;
import java.util.List;

public class ChannelsDecoder {

    public static List<Channel> transform(String xml) throws Exception {
        DIDLParser parser = new DIDLParser();
        DIDLContent content = parser.parse(xml);
        ChannelDecoder channelDecoder = new ChannelDecoder();
        List<Channel> channels = Lists.transform(content.getItems(), channelDecoder);
        channels = new ArrayList<>(channels); // Lists.transform is lazy evaluated, this code will make the list evaluated.
        return channels;
    }
}
