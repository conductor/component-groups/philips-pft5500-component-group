package com.github.soshibby.philips.pft5500.proxy;

import com.github.soshibby.philips.pft5500.proxy.registry.ResponseRegistry;
import com.github.soshibby.philips.pft5500.proxy.registry.TokenRegistry;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

public class RequestHandler extends HttpHandler {

    private static final Logger log = LoggerFactory.getLogger(RequestHandler.class);
    private ResponseRegistry responseRegistry;
    private TokenRegistry tokenRegistry;
    private URL remoteURL;

    public RequestHandler(URL remoteUrl, ResponseRegistry responseRegistry, TokenRegistry tokenRegistry) {
        this.responseRegistry = responseRegistry;
        this.tokenRegistry = tokenRegistry;
        this.remoteURL = remoteUrl;
    }

    @Override
    public void service(Request request, Response response) {
        try {
            String token = request.getParameter("token");
            log.info("Request received.");

            if (tokenRegistry.isTokenValid(token)) {
                log.info("Token '{}' is valid.", token);
            } else {
                log.info("Token '{}' is INVALID.", token);
                return;
            }

            this.responseRegistry.add(token, response);

            CloseableHttpClient httpclient = HttpClients.createDefault();
            HttpGet httpget = new HttpGet(this.remoteURL.toString());

            CloseableHttpResponse resp = httpclient.execute(httpget);

            copyStream(resp.getEntity().getContent(), response.getOutputStream());

            response.getWriter().flush();
            response.getWriter().close();
            resp.close();
        } catch (Exception e) {
            log.error("An http error occured.", e);
        }
    }

    public void setRemoteURL(URL url) {
        this.remoteURL = url;
    }

    public static void copyStream(InputStream input, OutputStream output) throws IOException {
        byte[] buffer = new byte[8192]; // Adjust if you want
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }
}
