package com.github.soshibby.philips.pft5500.integration.types.interfaces;

import com.github.soshibby.philips.pft5500.integration.types.Channel;

public interface CurrentChannelResponse {

    public void success(Channel channel);

    public void failure(String message);

}
