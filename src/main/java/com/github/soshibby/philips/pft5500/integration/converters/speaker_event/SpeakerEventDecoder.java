package com.github.soshibby.philips.pft5500.integration.converters.speaker_event;

import com.github.soshibby.philips.pft5500.integration.types.interfaces.SpeakerEvent;
import org.fourthline.cling.model.state.StateVariableValue;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.util.Map;

public class SpeakerEventDecoder {

    public static SpeakerEvent transform(Map<String, StateVariableValue> values) throws JAXBException {
        Event event = parseXml((String) values.get("LastChange").getValue());

        SpeakerEvent speakerEvent = new SpeakerEvent();
        speakerEvent.setMute(event.getInstanceID().getMute().getVal() == 1);
        speakerEvent.setVolume(event.getInstanceID().getVolume().getVal());

        return speakerEvent;
    }

    private static Event parseXml(String xml) throws JAXBException {
        StringReader reader = new StringReader(xml);
        JAXBContext jaxbContext = JAXBContext.newInstance(Event.class);
        Unmarshaller jaxbUnmarshaller;
        jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        return (Event) jaxbUnmarshaller.unmarshal(reader);
    }
}
