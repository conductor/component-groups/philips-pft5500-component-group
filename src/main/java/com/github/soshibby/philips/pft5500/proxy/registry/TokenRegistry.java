package com.github.soshibby.philips.pft5500.proxy.registry;

import java.util.*;

public class TokenRegistry {
    private Map<String, Date> tokens = new HashMap<String, Date>();

    public void add(String token, Date validTo) {
        tokens.put(token, validTo);
    }

    public void remove(String token) {
        tokens.remove(token);
    }

    public boolean isTokenValid(String token) {
        return !hasExpired(token);
    }

    public List<String> getExpiredTokens() {
        List<String> expiredTokens = new ArrayList<String>();

        for (String token : tokens.keySet()) {
            if (hasExpired(token)) {
                expiredTokens.add(token);
            }
        }

        return expiredTokens;
    }

    private boolean hasExpired(String token) {
        Date date = tokens.get(token);

        if (date == null) {
            return true;
        }

        if (date.before(new Date())) {
            return true;
        }

        return false;
    }

}
